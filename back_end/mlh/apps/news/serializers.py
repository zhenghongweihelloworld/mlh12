from rest_framework import serializers
from news.models import News, Comment, Category
from users.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"

class NewsSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    category = CategorySerializer()
    class Meta:
        model = News
        fields = ("id","title","author","category","create_time","content")


class DetailSerializer(serializers.ModelSerializer):
    # category = CategorySerializer()
    author = UserSerializer()
    """摘要怎么处理"""
    class Meta:
        model = News
        fields = ("id","title","author","create_time","is_collected","comments","content","category")
        read_only_fields = ('id',"comments","is_collected","create_time","author")
        # extra_kwargs = {
        #     'comments': {
        #         'required': True,
        #         'min_value': 0
#         #     }
#         # }
    def create(self, validated_data):
        user = self.context['request'].user
        if user:
            validated_data['author'] = user
        else:
            return ("用户不存在")
        news = News.objects.create(**validated_data)
        return news


# class CommentSerializer(serializers.ModelSerializer):
#     news = NewsSerializer
#     class Meta:
#         model = Comment
#         fields = ("id","user","content","count")
#
# class SubCommentSerializer(serializers.ModelSerializer):
#     news = NewsSerializer()
#     subs = CommentSerializer(many=True, read_only=True)
#     class Meta:
#         model = Comment
#         fields = ("id","user","content","subs","count")
        # read_only_fields = ('id',)
        # extra_kwargs = {
        #     'news': {
        #         'write_only': True
        #     }
        # }

    # def create(self, validated_data):
    #
    #     instance = super(serializers.ModelSerializer, self).create(validated_data)
    #
    #     user = self.context['request'].user
    #     if user:
    #         instance.author = user
    #     else:
    #         return ("用户不存在")
    #     news_id = self.context["view"].kwargs.get("news_id")
    #     news = News.objects.get(news_id)
    #
    #     if validated_data["parent_id"]:
    #         instance.user=user
    #         instance.news=news
    #         instance.content=validated_data['content']
    #         instance.count=0
    #         instance.parent_id=validated_data["parent_id"]
    #
    #         parent_comment = Comment.objects.get(id=validated_data["parent_id"])
    #         parent_comment.count += 1
    #         parent_comment.save()
    #     else:
    #             instance.user=user,
    #             instance.news=news,
    #             instance.content=validated_data['content'],
    #             instance.count=0
    #
    #     news.comments += 1
    #     news.save()
    #
    #     instance.save()
    #     return instance



# class SubmitSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = News
#         fields = ("content","title","category","user")
#         read_only_fields = ('id',)
#         extra_kwargs = {
#             'count': {
#                 'required': True,
#                 'min_value': 0
#             }
#         }
#
#     def create(self, validated_data):
#         instance = super(serializers.ModelSerializer, self).create(validated_data)
#
#         user = self.context['request'].user
#         instance.user = user
#
#         instance.save()
#         return instance


class CommentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    class Meta:
        model = Comment
        fields = ("id","content","news","user","count","parent")
        read_only_fields = ("id","count","parent")
        extra_kwargs = {
                    'news': {
                        'required': True, # required 与write_only冲突吗
                        "write_only":True

                    }
                }
    def create(self, validated_data):
        user = self.context['request'].user
        news_id = self.context['view'].get('news_id')
        news = News.objects.get(id=news_id)
        validated_data["user"] = user
        validated_data['news'] = news
        comment = Comment.objects.create(**validated_data)
        return comment



