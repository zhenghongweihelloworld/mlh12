from django.test import TestCase

# Create your tests here.
from news.models import News, Comment

news = News.objects.get(id=1)
for i in range(100):
    news.title = str(i)
    news.id = None
    news.save()

comment = Comment.objects.get(id=1)
for i in range(10):
    comment.id =None
    comment.content = str(i)
    comment.save()