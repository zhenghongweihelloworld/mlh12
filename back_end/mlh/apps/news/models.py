# Create your models here.
from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

from news.utils import BaseModel
from users.models import User


class Category(models.Model):
    """新闻种类表"""
    name = models.CharField(max_length=50,verbose_name="种类")
    sequence = models.IntegerField(verbose_name='排序')
    class Meta:
        db_table = 'tb_category'
        verbose_name = '种类'
        verbose_name_plural = verbose_name


class News(BaseModel):
    """新闻详情表"""
    title = models.CharField(max_length=32,verbose_name="新闻标题")
    content = RichTextUploadingField(verbose_name='新闻详情')
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='新闻发布者')
    category = models.ForeignKey(Category,on_delete=models.CASCADE,verbose_name="新闻种类")
    comments = models.IntegerField(default=0,verbose_name="新闻的评论量",null=True)
    is_collected = models.BooleanField(default=False,verbose_name="是否收藏")
    clicks = models.IntegerField(verbose_name="点击量",default=0)
    # digest = models.CharField(verbose_name="摘要")
    # image_url = models.CharField(verbose_name="图片链接")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    class Meta:
        db_table = "tb_news"
        verbose_name = "新闻详情"
        verbose_name_plural = verbose_name




class Comment(BaseModel):
    """评论表"""
    user = models.ForeignKey(User,verbose_name="用户")
    news = models.ForeignKey(News,verbose_name="新闻")
    parent = models.ForeignKey("self",default=None,null=True,blank=True,verbose_name="父评论")
    content = models.TextField(verbose_name="评论内容")
    count = models.IntegerField(default=0,verbose_name="评论量")
    class Meta:
        db_table = "tb_comment"
        verbose_name = "评论"
        verbose_name_plural = verbose_name


