from django.contrib import admin

# Register your models here.
from news import models

admin.site.register(models.Category)
admin.site.register(models.Comment)
admin.site.register(models.News)

