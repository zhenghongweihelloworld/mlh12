from django.conf.urls import url

from news import views

urlpatterns  = [
    url(r'^category/$',views.CategoryView.as_view()),
    url(r"^news/$",views.NewsView.as_view()),
    url(r'detail/(?P<pk>\d+)/$',views.NewsSingleView.as_view()),
    url(r'news/submition/$',views.SubmitView.as_view()),
    url(r'comments/$',views.SubmitView.as_view()),
    url(r'comment/(?P<news_id>\d+)/$',views.CommentView2.as_view()),
    url(r'comment/sub/(?P<news_id>\d+)$',views.CommentView2.as_view())
]

