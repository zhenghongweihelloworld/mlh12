from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import spits, spits_comment, spits_Like, spits_collection, spits_comment_Like


admin.site.register(spits)
admin.site.register(spits_comment)
admin.site.register(spits_Like)
admin.site.register(spits_collection)
admin.site.register(spits_comment_Like)