from django.db import models

# Create your models here.


class spits(models.Model):
    content = models.TextField(verbose_name='吐槽内容')
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='spitslot', verbose_name='作者')
    sp_like_count = models.IntegerField(verbose_name='吐槽点赞次数')
    share_count = models.IntegerField(verbose_name='分享量')
    index_image_url = models.ImageField(upload_to='spitslotpackge', verbose_name='吐槽图片')
    is_collected = models.BooleanField(default=0, verbose_name='收藏状态')
    is_like = models.BooleanField(default=0, verbose_name='点赞状态')
    is_cryptonym = models.BooleanField(default=0, verbose_name='匿名状态')
    is_delete = models.BooleanField(default=True, verbose_name='逻辑删除')
    create_time = models.DateField(verbose_name='创建时间')

    class Meta:
        db_table = 'tb_spitslot'
        verbose_name = '吐槽'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "吐槽"


class spits_comment(models.Model):
    spits_content = models.TextField(verbose_name='吐槽评论内容')
    like_count = models.IntegerField(verbose_name='点赞量')
    user_id = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='spitslot_comment', verbose_name='用户')
    spits_id = models.ForeignKey('spits', on_delete=models.CASCADE, related_name='spitslot_comment', verbose_name='用户')
    is_like = models.BooleanField(default=0, verbose_name='点赞状态')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    class Meta:
        db_table = 'tb_spit_comment'
        verbose_name = '吐槽评论'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "吐槽评论"


class share(models.Model):
    user_id = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='user_share', verbose_name='分享的用户')
    spits_id = models.ForeignKey('spits', on_delete=models.CASCADE, related_name='sp_share', verbose_name='分享的吐槽')

    class Meta:
        db_table = 'tb_spit_share'
        verbose_name = '分享'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "吐槽分享"


class spits_collection(models.Model):
    user_id = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='user_collection',
                             verbose_name='收藏的用户')
    spits_id = models.ForeignKey('spits', on_delete=models.CASCADE, related_name='spits_collection',
                                 verbose_name='收藏的吐槽')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    class Meta:
        db_table = 'tb_spit_collection'
        verbose_name = '吐槽收藏'
        verbose_name_plural = verbose_name


class spits_Like(models.Model):
    user_id = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='user_spits_like', verbose_name='点赞的用户')
    spits_id = models.ForeignKey('spits', on_delete=models.CASCADE, related_name='spits_like', verbose_name='点赞的吐槽')

    class Meta:
        db_table = 'tb_spit_like'
        verbose_name = '吐槽点赞'
        verbose_name_plural = verbose_name


class spits_comment_Like(models.Model):
    user_id = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='user_spitscomment_like', verbose_name='点赞的用户')
    spits_id = models.ForeignKey('spits', on_delete=models.CASCADE, related_name='spits_spitscomment_like', verbose_name='点赞的吐槽')
    comment_id = models.ForeignKey('users.User', on_delete=models.CASCADE, verbose_name="评论id")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    class Meta:
        db_table = 'tb_spit_comment_like'
        verbose_name = '吐槽评论点赞'
        verbose_name_plural = verbose_name