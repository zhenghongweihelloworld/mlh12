from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from . import views

urlpatterns = [
    url(r'^spit-index/$', views.SpitList.as_view()),
    url(r'^spit-detail/$', views.SpitDetail.as_view()),
    url(r'^spit-submit/$', views.Spit_Submit.as_view()),
    url(r'^spit_like/$', views.Spit_like.as_view()),
    url(r'^spit_collection/$', views.Spit_colletion.as_view()),
    url(r'^spit_like_action/$', views.SpitLikeActionView.as_view()),
    url(r'^spit_collection_action/$', views.SpitCollectionActionView.as_view()),
    url(r'^spit_comment_like/$', views.SpitCommentLikeView.as_view()),
    url(r'^spit_comment_like_action/$', views.SpitCommentLikeActionView.as_view()),
    url(r'^spit_comment_reply/$', views.SpitCommentCreateView.as_view()),
]

router = DefaultRouter()
router.register('spit_comment', views.Spit_Comment, base_name='spit_comment')

urlpatterns += router.urls