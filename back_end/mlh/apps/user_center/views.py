from django.shortcuts import render

# Create your views here.

from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet

from .models import MyCenter
from .serializers import MyCenterSerializer

# class UserCenterView(RetrieveAPIView):
#     queryset = MyCenter.objects.all()
#     serializer_class = MyCenterSerializer
#
#     def get(self, request, pk):
#         return self.retrieve(request)
#
#


class UserCenterViewSet(ListAPIView):
    queryset = MyCenter.objects.all()
    serializer_class = MyCenterSerializer
    # def get_serializer_class(self):
    #     return MyCenterSerializer
