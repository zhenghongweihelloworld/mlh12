from rest_framework import serializers

from .models import MyCenter

class MyCenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyCenter
        fields = "__all__"