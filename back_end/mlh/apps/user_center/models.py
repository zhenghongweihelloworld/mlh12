from django.db import models

# Create your models here.

class MyCenter(models.Model):

    class GENDER():
        MAN, WOMAN= range(1, 3)
        DESC_MAN, DESC_WOMAN =  "男", "女"
        CHOICE = zip((MAN,WOMAN), (DESC_MAN, DESC_WOMAN))
    # user_id = models.ForeignKey(  )
    current_adress = models.CharField(verbose_name='居住地址', max_length=128)
    university = models.CharField(verbose_name='毕业院校', max_length=64)
    corporation = models.CharField(verbose_name='公司', max_length=32)
    website = models.CharField(verbose_name='个人网页', max_length=128)
    auto_image = models.ImageField(verbose_name='用户图像')
    name = models.CharField(verbose_name='昵称', max_length=32)
    phone = models.CharField(verbose_name='电话', max_length=32)
    gender= models.SmallIntegerField(verbose_name='性别', choices=GENDER.CHOICE)
    age = models.IntegerField(verbose_name="年龄",max_length=32)
    work_experience = models.TextField(verbose_name='工作经历',null=True)
    edu_experience = models.TextField(verbose_name='教育经历' ,null=True)
    skill = models.TextField(verbose_name='擅长技能',null=True)
    is_delete = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'user_center'
        verbose_name = '个人中心'
        verbose_name_plural = verbose_name

