from django.db import models

# Create your models here.
from users.models import User


class Questions(models.Model):
    """问题表"""
    q_user = models.ForeignKey(to=User,verbose_name='问题作者ID',on_delete=models.CASCADE)
    # look_count = models.IntegerField(verbose_name='阅读次数',default=0)
    like_count = models.IntegerField(verbose_name='得票次数',default=0)
    q_text = models.TextField(verbose_name='问题内容')
    titie = models.CharField(max_length=64,verbose_name='问题标题')
    create_time = models.TimeField(verbose_name='问题创建时间')
    commit_count = models.IntegerField(verbose_name='问题评论数量',default=0)
    labels = models.ManyToManyField(to="Labels",db_table='q_l')
    q_like_count = models.IntegerField(default=0,verbose_name="问题被点赞次数")

    class Meta:
        db_table = 'tb_questions'
        verbose_name = '问题'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.titie


class Commits(models.Model):
    """评论表"""
    c_user = models.ForeignKey(User,verbose_name='问题作者ID',on_delete=models.CASCADE)
    c_question = models.ForeignKey(to=Questions,on_delete=models.CASCADE,verbose_name="评论所属内容")
    c_text = models.TextField(verbose_name='评论内容')
    c_like_count = models.IntegerField(default=0,verbose_name="评论被点赞次数")


    class Meta:
        db_table='tb_commit'
        verbose_name = '评论'
        verbose_name_plural = verbose_name

    def __str__(self):
        return "评论表对象"

class Labels(models.Model):
    """标签表"""
    l_title = models.CharField(max_length=10)


