import json

from django.shortcuts import render

# Create your views here.
# GET http://127.0.0.1:8000:
from rest_framework.generics import ListAPIView, GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ReadOnlyModelViewSet

from questions.models import Questions, Commits
from questions.serializers import QuestionSerializer, CommentsSerializer


#
# class QuestionView(APIView):
#
#
#
#     def get(self,request):
#         # questionsss = questions.objects.filter(id=2)
#         qs = Questions.objects.all()
#         serializer = QuestionSerializer(qs,many=True)
#         return Response(serializer.data)


class QAView(ReadOnlyModelViewSet):
    queryset = Questions.objects.all()
    serializer_class = QuestionSerializer

    # def retrieve(self, request, *args, **kwargs):
    #     serializer_class =


class CommentsView(ListAPIView):
    serializer_class = CommentsSerializer

    def get_queryset(self):
        question_id = self.kwargs['question_id']
        return Commits.objects.filter(c_question = question_id)










