from django.contrib import admin

# Register your models here.
from questions import models
#
admin.site.register(models.Questions)
admin.site.register(models.Commits)
admin.site.register(models.Labels)
