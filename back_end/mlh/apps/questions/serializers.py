# -*-coding:utf-8-*-
from rest_framework import serializers

from questions.models import Questions, Commits, Labels
from users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',)


class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Labels
        fields = ('l_title',)


class CommentsSerializer(serializers.ModelSerializer):
    c_user = UserSerializer

    class Meta:
        model = Commits
        fields = ('c_text', 'c_user','c_like_count')


class AddCommentLikeCountSerializer(serializers.ModelSerializer):
    pass




class QuestionSerializer(serializers.ModelSerializer):
    q_user = UserSerializer()
    labels = LabelSerializer(many=True)

    class Meta:
        model = Questions
        fields = '__all__'
