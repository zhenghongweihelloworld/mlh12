# -*-coding:utf-8-*-
from django.conf.urls import url

from questions import views

urlpatterns = [url(r'^questions/$', views.QAView.as_view({'get': 'list'})),
               url(r'^questions/(?P<pk>\d+)/$', views.QAView.as_view({'get': 'retrieve'})),
               url(r'^comments/(?P<question_id>\d+)', views.CommentsView.as_view())
               ]
