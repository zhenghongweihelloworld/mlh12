from django.db import models

# Create your models here.
class Activity(models.Model):
    # STATUS_CHOICE = ((1,'立即报名'),(2,'报名截止'),(3,'活动以结束'))
    class STATUS():
        TO_APPLY, NO_APPLY, FINISHED = range(1,4)
        DESC_TO_APPLY, DESC_NO_APPLY, DESC_FINISHED = "立即报名", "报名截止", "活动已结束"
        CHOICE = zip((TO_APPLY, NO_APPLY, FINISHED),(DESC_TO_APPLY, DESC_NO_APPLY, DESC_FINISHED))

    image = models.CharField(verbose_name='图片',max_length=128)
    title = models.CharField(verbose_name='标题',max_length=32)
    city = models.CharField(verbose_name='举办城市' ,max_length=64)
    adress= models.CharField(verbose_name='地址',max_length=128)
    start_time = models.DateTimeField(verbose_name='开始时间')
    end_time = models.DateTimeField(verbose_name='结束时间')
    apply_time = models.DateTimeField(verbose_name='报名截止时间')
    status = models.SmallIntegerField(choices=STATUS.CHOICE,verbose_name='')
    aponsor = models.CharField(verbose_name='举办方',max_length=64)
    description = models.TextField(verbose_name='详情')
    abstract = models.TextField(verbose_name='简介')
    link = models.CharField(verbose_name='链接',max_length=128)
    is_delete = models.BooleanField(default=False)



    def __str__(self):
        return self.title

    class Meta:
        db_table = 'activity'
        verbose_name = '活动'
        verbose_name_plural = verbose_name


