from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^activitys/$',views.ActivityViewSet.as_view({'get':'list'})),
    url(r'^activitys/(?P<pk>\d+)$', views.ActivityViewSet.as_view({'get':'retrieve'})),
]