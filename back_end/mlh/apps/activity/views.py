from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
from .models import Activity
from rest_framework import serializers

# Create your views here.

class ActivityListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Activity
        fields = ('id', 'title','image', 'city', 'start_time', 'status','apply_time')

class ActivityDetaiSerializers(serializers.ModelSerializer):

    class Meta:
        model = Activity
        exclude = ('is_delete',)


class ActivityViewSet(ReadOnlyModelViewSet):
    queryset = Activity.objects.filter(is_delete=False).order_by('-start_time')

    def get_serializer_class(self):
        if self.action == 'list':

            return ActivityListSerializer

        elif self.action == 'retrieve':
            return ActivityDetaiSerializers