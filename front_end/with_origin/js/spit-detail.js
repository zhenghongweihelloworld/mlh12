var vm = new Vue({ 
    el:'#spit_app',
    data:{
        host: host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        spit: [],
        spit_id: '',
        spit_url: '',
        spit_comments: [],
        spit_likes:[],
        comment_likes:[],
        message: "",

    },
    mounted:function () {
        // this.get_spit_id();
        this.spit_url = this.get_query_string('spit_id');
        axios.get(this.host + '/spit-detail/?spit_id=' + this.spit_url, {
            responseType:'json'
        }).then(response => {
            this.spit = response.data
        })
        .catch(error => {
            console.log(error.response.data)
        });
        this.get_spit_comment();
        if (this.user_id && this.token) {
            this.get_spit_like();
            this.get_spit_comment_like();
        }

    },
    methods: {
        // get_spit_id: function(){
        //     var re = /^\/spit-detail\/(\d+).html$/;
        //     this.spit_id = document.location.pathname.match(re)[1];
        // },
        // 获取url路径参数
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        },
        get_spit_comment: function () {
            axios.get(this.host + '/spit_comment/?spit_id=' + this.spit_url, {
            responseType:'json'
        }).then(response => {
            this.spit_comments = response.data
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        get_spit_like: function () {
            axios.get(this.host + '/spit_like/?user_id=' + this.user_id, {
                headers: {
                'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
        }).then(response => {
            this.spit_likes = response.data['spit_likes'];
            console.log(this.spit_likes)
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        like: function (spit_id) {
        axios.post(this.host + '/spit_like_action/',{
                spit_id:spit_id,
                action:'add'
            } ,{
            headers: {
                'Authorization': 'JWT ' + this.token
            },
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        notlike:function (spit_id) {
        axios.post(this.host + '/spit_like_action/',{
                spit_id:spit_id,
                action:'cancel'
            } ,{
            headers: {
                'Authorization': 'JWT ' + this.token
            },
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        get_spit_comment_like: function () {
            axios.get(this.host + '/spit_comment_like/?user_id=' + this.user_id, {
                headers: {
                'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
        }).then(response => {
            this.comment_likes = response.data['comment_likes'];

        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        commentnotlike: function (comment_id) {
            axios.post(this.host + '/spit_comment_like_action/',{
                comment_id:comment_id,
                action:'cancel'
                } ,{
                headers: {
                'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        commentlike: function (comment_id) {
            axios.post(this.host + '/spit_comment_like_action/',{
                comment_id:comment_id,
                action:'add'
                } ,{
                headers: {
                'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        reply: function (spit_id) {
            axios.post(this.host + '/spit_comment_reply/',{
                spit_id: spit_id,
                content: this.message
                } ,{
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
        }).then(response => {
            alert('发送成功')
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        }
    }
})