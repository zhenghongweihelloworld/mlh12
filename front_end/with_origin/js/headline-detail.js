// /**
//  * Created by python on 19-1-15.
//  */
var vm = new Vue({
    el: "#app",
    data: {
        // 页面中需要使用到的数据，键值对
        news: null,
        id: 1,
        comment: [],
        comments_parent: [],
        comments_son: [],
    },

    mounted: function () {

        this.id = get_query_string("id", 1);
        axios.get(host + "/detail/" + this.id)
            .then(response => {
                this.news = response.data;
                // console.log(response.data)
                axios.get(host + "/comment/" + this.id)
                    .then(response => {
                        this.comments = response.data;
                        for (var i = 0; i < this.comments.length; i++) {
                            if (this.comments[i].parent == null) {
                                this.comments_son.push(this.comments[i])
                            }
                            else {
                                this.comments_parent.push(this.comments[i])
                            }
                        }
                        console.log(response.data)

                    })
            });


    },
    methods: {
        to_reply: function (even) {
            $(even.target).parents(".text").siblings(".edit-box").toggle();
        }
    }
});

