var vm = new Vue({
    el: "#headline",
    data: {
        // 页面中需要使用到的数据，键值对
        names: null,
        news: [],
        isActive: true,
        hasError: false,
        isFans: true,
        category_id: 0,
        activeClass: get_query_string("category", "热门")


    },

    mounted: function () {
        // 一加载就需要做的，直接是代码
        this.category_id = get_query_string("category_id", 1);
        axios.get(host + "/category/")
            .then(response => {
                this.names = response.data;
                console.log(response.data)
            })
            .catch(error => {

            });

        // axios.get(host + "/news/")
        //     .then(response => {
        //         this.news = response.data;
        //         // console.log(response.data)
        //
        //     });

        axios.get(host + "/news/?category_id=" + this.category_id)
            .then(response=>{
                this.news = response.data;
                // console.log(response.data)

            })

    },
})
