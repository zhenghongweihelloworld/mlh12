/**
 * Created by python on 19-1-16.
 */
var app = new Vue({
    el:"#app",
    data:{
        // 页面中需要使用到的数据，键值对
         user_center: [],
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
       axios.get(host + '/' + "user_center")
       .then(response=>{
            this.user_center = response.data
       })
       .catch(error =>{})
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})