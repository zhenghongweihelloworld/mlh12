/**
 * Created by python on 19-1-11.
 */

var app = new Vue({
    el:"#app",
    data:{
        // 页面中需要使用到的数据，键值对
        activity: null,
        deadline: 0,
        now: 0,
        remain: 0,
        status:0,

    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数

    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        axios.get(host + '/' + "activitys/"+get_query_string('id',1))
        .then(response=>{

            this.activity = response.data
        //    开启定时器　得到一个数字　转化为时分秒
        //    倒计时＝截止时间－当前时间
            this.deadline = Math.floor(new Date(response.data.apply_time).getTime()/1000);
            let server_now = response.headers.date;    //获取服务器当前时间
            this.now = Math.floor(new Date(server_now).getTime()/1000);
            this.remain = this.deadline - this.now;
            remain = this.remain
            console.log(remain)
            A = setInterval( () => {
                this.now += 1;
                this.remain = this.deadline - this.now;
            },1000)
             if (remain <= 0){
                this.status = 1 ;
                clearInterval(A)

            }


        })

        .catch(error =>{})
    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    },
    filters:{
        second_to_dhms:function (value) {
            let day = Math.floor(value / (60 * 60 * 24));
            let hour = Math.floor((value / 60 / 60 ) % 24) ;
            let minute = Math.floor((value / 60 ) % 60);
            let second = Math.floor(value % 60);
            return day + "天" + hour + "时" + minute + "分" + second + "秒"
        }
    },

})
