/**
 * Created by python on 19-1-9.
 */
const host = 'http://127.0.0.1:8000';

function get_query_string(name, def) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return decodeURI(r[2]);
    }
    return def;
};
//str_to_date字符串转日期
//date_to_ymd获取日期的年月日
//date_to_hms获取日期的时分秒
//date_to_week获取日期的 星期几

Vue.filter('str_to_date',function (value) {
    return new Date(value)
});

Vue.filter('date_to_ymd',function (value) {
    return value.getFullYear() + "-" + (value.getMonth()+1) + "-" + value.getDate()
});

Vue.filter('date_to_hm',function (value) {
    return value.getHours() + "-" + value.getMinutes()
});

const WEEKS = ['周日','周一','周二','周三','周四','周五','周六']

Vue.filter('date_to_week',function (value) {
    return WEEKS[value.getDay()]
});

Vue.filter('temp_status',function (value) {
    if (value > 0){
        return "立即报名"
    }
    else {
        return "活动已结束"
    }
});