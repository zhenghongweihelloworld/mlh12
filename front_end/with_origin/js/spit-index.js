var vm = new Vue({
    el:'#spit_app',
    data:{
        host: host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        page: 1, // 当前页数
        page_size: 5, // 每页数量
        count: 0,  // 总数量
        spits: [],
        spit_likes:[],
        spit_collections:[],
        action:""

    },
    computed: {
        total_page: function(){  // 总页数
            return Math.ceil(this.count/this.page_size);
        },
        next: function(){  // 下一页 
            if (this.page >= this.total_page) {
                return 0;
            } else {
                return this.page + 1;
            }
        },
        previous: function(){  // 上一页
            if (this.page <= 0 ) {
                return 0;
            } else {
                return this.page - 1;
            }
        },
        page_nums: function(){  // 页码
            // 分页页数显示计算
            // 1.如果总页数<=5
            // 2.如果当前页是前3页
            // 3.如果当前页是后3页,
            // 4.既不是前3页，也不是后3页
            var nums = [];
            if (this.total_page <= 5) {
                for (var i=1; i<=this.total_page; i++){
                    nums.push(i);
                }
            } else if (this.page <= 3) {
                nums = [1, 2, 3, 4, 5];
            } else if (this.total_page - this.page <= 2) {
                for (var i=this.total_page; i>this.total_page-5; i--) {
                    nums.push(i);
                }
            } else {
                for (var i=this.page-2; i<this.page+3; i++){
                    nums.push(i);
                }
            }
            return nums;
        }
    },
    mounted:function () {
        this.get_spit()
    },
    methods: {
        get_spit:function () {
            axios.get(this.host + '/spit-index/', {
            params: {
                page: this.page,
                page_size: this.page_size,
                // ordering: this.ordering
                    },
            responseType:'json'
        }).then(response => {
            this.count = response.data.count;
            this.spits = response.data.results;
        })
        .catch(error => {
            console.log(error.response.data)
        });
        if (this.user_id && this.token) {
            this.get_spit_like();
            this.get_spit_collection()
        }
        },
        like: function (spit_id) {
        axios.post(this.host + '/spit_like_action/',{
                spit_id:spit_id,
                action:'add'
            } ,{
            headers: {
                'Authorization': 'JWT ' + this.token
            },
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        notlike:function (spit_id) {
        axios.post(this.host + '/spit_like_action/',{
                spit_id:spit_id,
                action:'cancel'
            } ,{
            headers: {
                'Authorization': 'JWT ' + this.token
            },
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        collection: function (spit_id) {
            axios.post(this.host + '/spit_collection_action/',{
                spit_id:spit_id,
                action:'add'
            } ,{
                headers: {
                    'Authorization': 'JWT ' + this.token
                },
                responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        cancelcollection: function (spit_id) {
        axios.post(this.host + '/spit_collection_action/',{
                spit_id:spit_id,
                action:'cancel'
            } ,{
            headers: {
                'Authorization': 'JWT ' + this.token
            },
            responseType:'json'
        }).then(response => {
            window.location.reload()
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        get_spit_detail: function (spit_id) {
            window.location.href="http://127.0.0.1:8080/spit-detail.html?spit_id=" + spit_id;
        },
        get_spit_like: function () {
            axios.get(this.host + '/spit_like/?user_id=' + this.user_id, {
                headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                responseType:'json'
        }).then(response => {
            this.spit_likes = response.data['spit_likes'];
            console.log(this.spit_likes)
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        get_spit_collection: function () {
        axios.get(this.host + '/spit_collection/?user_id=' + this.user_id, {
            headers: {
                'Authorization': 'JWT ' + this.token
            },
            responseType:'json'
        }).then(response => {
            this.spit_collections = response.data['spit_collections']
        })
        .catch(error => {
            console.log(error.response.data)
        });
        },
        // 点击页数
        on_page: function(num){
            if (num != this.page){
                this.page = num;
                this.get_spit()
            }
        },
    }
})